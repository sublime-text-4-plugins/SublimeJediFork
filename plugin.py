# -*- coding: utf-8 -*-
import os
import sys

# add dependencies on package initialization
sys.path.append(os.path.join(os.path.dirname(__file__), "dependencies"))

from .plugins import SublimeJediForkToggleLoggingLevelCommand             # noqa
from .plugins import events
from .plugins import settings                                             # noqa

# NOTE: Import last.
from .plugins.completion import Autocomplete                              # noqa
from .plugins.completion import SublimeJediParamsAutocomplete             # noqa
from .plugins.go_to import SublimeJediEventListener                       # noqa
from .plugins.go_to import SublimeJediFindUsages                          # noqa
from .plugins.go_to import SublimeJediGoto                                # noqa
from .plugins.helper import HelpMessageCommand                            # noqa
from .plugins.helper import SublimeJediDocstring                          # noqa
from .plugins.helper import SublimeJediSignature                          # noqa
from .plugins.helper import SublimeJediTooltip                            # noqa


def plugin_loaded():
    """On plugin loaded callback.
    """
    events.broadcast("plugin_loaded")


def plugin_unloaded():
    """On plugin unloaded.
    """
    events.broadcast("plugin_unloaded")


if __name__ == "__main__":
    pass

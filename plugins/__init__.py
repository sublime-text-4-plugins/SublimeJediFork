# -*- coding: utf-8 -*-
import os

root_folder = os.path.realpath(
    os.path.abspath(
        os.path.join(
            os.path.normpath(os.path.join(os.path.dirname(__file__), os.pardir))
        )
    )
)

import sublime  # noqa
import sublime_plugin

from python_utils import log_system
from python_utils.sublime_text_utils import logger as logger_utils
from python_utils.sublime_text_utils import settings as settings_utils
from python_utils.sublime_text_utils.events import Events
from python_utils.sublime_text_utils.queue import Queue

queue = Queue()
events = Events()

_log_file = log_system.generate_log_path(
    storage_dir=os.path.join(root_folder, "tmp", "logs"), prefix="LOG"
)
plugin_name = "SublimeJediFork"
plugin_id = "SublimeJediFork-{}"
logger = logger_utils.SublimeLogger(logger_name=plugin_name, log_file=_log_file)
settings = settings_utils.SettingsManager(
    settings_file=plugin_name,
    events=events,
    logger=logger,
)


def set_logging_level():
    try:
        logger.set_logging_level(logging_level=settings.get("logging_level", "ERROR"))
    except Exception as err:
        print(__file__, err)


@events.on("plugin_loaded")
def on_plugin_loaded():
    """On plugin loaded."""
    queue.debounce(
        settings.load, delay=100, key=f"{plugin_name}-debounce-settings-load"
    )
    queue.debounce(
        set_logging_level, delay=200, key=f"{plugin_name}-debounce-set-logging-level"
    )


@events.on("plugin_unloaded")
def on_plugin_unloaded():
    settings.unobserve()
    queue.unload()
    events.destroy()


@events.on("settings_changed")
def on_settings_changed(settings_obj, **kwargs):
    if settings_obj.has_changed("logging_level"):
        queue.debounce(
            set_logging_level, delay=200, key=f"{plugin_name}-debounce-settings-changed"
        )


class SublimeJediForkToggleLoggingLevelCommand(
    settings_utils.SettingsToggleList, sublime_plugin.ApplicationCommand
):
    def __init__(self, *args, **kwargs):
        sublime_plugin.ApplicationCommand.__init__(self, *args, **kwargs)
        settings_utils.SettingsToggleList.__init__(
            self,
            key="logging_level",
            settings=settings,
            description="Logging level - {}",
            values_list=["ERROR", "DEBUG"],
        )


if __name__ == "__main__":
    pass

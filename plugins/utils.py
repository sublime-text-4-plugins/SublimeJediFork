# -*- coding: utf-8 -*-
import os

import sublime
import sublime_plugin  # noqa

from . import settings


class PythonCommandMixin(object):
    """A mixin that hides and disables command for non-python code"""

    def is_visible(self):
        """The command is visible only for python code"""
        return is_desired_scope(self.view)

    def is_enabled(self):
        """The command is enabled only when it is visible"""
        return self.is_visible()


def is_desired_scope(view):
    try:
        return view.match_selector(
            view.sel()[0].begin(), settings.get("commands_scope", "source.python")
        )
    except IndexError:
        return False


def is_repl(view):
    """Check if a view is a REPL."""
    return view.settings().get("repl", False)


def unique(items, pred=lambda x: x):
    stack = set()

    for i in items:
        calculated = pred(i)
        if calculated in stack:
            continue
        stack.add(calculated)
        yield i


def to_relative_path(path):
    """
    Trim project root pathes from **path** passed as argument

    If no any folders opened, path will be retuned unchanged
    """
    folders = sublime.active_window().folders()
    for folder in folders:
        # close path with separator
        if folder[-1] != os.path.sep:
            folder += os.path.sep

        if path and path.startswith(folder):
            return path.replace(folder, "")

    return path


def split_path(d, keys):
    assert isinstance(d, dict) and isinstance(keys, list)
    for k in [x for x in keys if d.get(x) and os.path.exists(d[x])]:
        d["%s_path" % k], d["%s_name" % k] = os.path.split(d[k])
        d["%s_base_name" % k], d["%s_extension" % k] = os.path.splitext(d["%s_name" % k])
        d["%s_extension" % k] = d["%s_extension" % k].lstrip(".")
    return d
